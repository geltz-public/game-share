#!/bin/bash
set -e # Exit on any failures

# Set up environment variables
STACK_NAME=game-share-setup;
REGION=`aws configure get region`;

# Validate user operations
OP="all";
if [ $# -eq 1 ]; then
  if [ "$1" == "aws" ]; then
    OP="aws";
  elif [ "$1" == "app" ]; then
    OP="app";
  elif [ "$1" == "amplify-config" ]; then
    OP="amplify-config";
  else
    echo "Usage: deploy.sh [aws|app|amplify-config|all]";
    exit 1;
  fi
fi

echo "Deploying the following components: $OP"

if [ "$OP" == "aws" ] || [ "$OP" == "all" ]; then
  echo "Finding cf template bucket in this region...";
  TEMPLATE_BUCKET=`aws s3api list-buckets --query "Buckets[?starts_with(Name, 'cf-templates') && ends_with(Name, '$REGION')].Name" --output text`;
  echo "Found bucket: $TEMPLATE_BUCKET";
  echo "Deploying setup stack...";
  aws cloudformation deploy --stack-name $STACK_NAME --template-file ./aws/setup.yml --s3-bucket $TEMPLATE_BUCKET --capabilities CAPABILITY_IAM;
fi

echo "Gathering deployment info...";
USER_POOL_ID=`aws cloudformation describe-stacks --stack-name $STACK_NAME --query "Stacks[0].Outputs[?OutputKey=='oUserPoolId'].OutputValue" --output text`;
USER_POOL_CLIENT_ID=`aws cloudformation describe-stacks --stack-name $STACK_NAME --query "Stacks[0].Outputs[?OutputKey=='oUserPoolClientId'].OutputValue" --output text`;
API_ID=`aws cloudformation describe-stacks --stack-name $STACK_NAME --query "Stacks[0].Outputs[?OutputKey=='oApiId'].OutputValue" --output text`;
API_URL=`aws cloudformation describe-stacks --stack-name $STACK_NAME --query "Stacks[0].Outputs[?OutputKey=='oApiUrl'].OutputValue" --output text`;
WEBSITE_BUCKET=`aws cloudformation describe-stacks --stack-name $STACK_NAME --query "Stacks[0].Outputs[?OutputKey=='oWebsiteBucket'].OutputValue" --output text`;
CLOUDFRONT_ID=`aws cloudformation describe-stacks --stack-name $STACK_NAME --query "Stacks[0].Outputs[?OutputKey=='oCloudFrontId'].OutputValue" --output text`;
CLOUDFRONT_DOMAIN=`aws cloudformation describe-stacks --stack-name $STACK_NAME --query "Stacks[0].Outputs[?OutputKey=='oCloudFrontDomain'].OutputValue" --output text`;

if [ "$OP" == "aws" ] || [ "$OP" == "all" ]; then
  echo "Updating the API deployment (id: $API_ID)";
  aws apigateway create-deployment --rest-api-id $API_ID --stage-name prod;
  echo "Done! AWS resources deployed. API available at: $API_URL";
fi

if [ "$OP" == "app" ] || [ "$OP" == "amplify-config" ] || [ "$OP" == "all" ]; then
  echo "Injecting Amplify config into UI...";
  cat > game-share/pages/amplify-config.json <<EOL
{
  "region": "$REGION",
  "userPoolId": "$USER_POOL_ID",
  "userPoolWebClientId": "$USER_POOL_CLIENT_ID",
  "apiEndpoint": "$API_URL"
}
EOL
fi

if [ "$OP" == "app" ] || [ "$OP" == "all" ]; then
  echo "Building the UI...";
  cd ./game-share;
  npm install;
  npm run build;
  cd ..;

  echo "Deploying UI to S3...";
  cd ./game-share/out;
  # Alright so we have to set the content type of our html files and remove the extensions for it to serve without issues

  # First we'll upload everything but the html files, the delete flag will clear out all files in the bucket that aren't
  # in the local directory, so this will remove the html files in the bucket since they don't have an extension there but
  # they do locally. This will cause a minor outage if the page isn't cached by CloudFront. The next step should run quick
  # enough that it won't be an issue.
  aws s3 sync --delete --exclude '*.html' . s3://$WEBSITE_BUCKET;
  # Then we'll upload the html files with the correct content type and remove the extensions
  HTML_FILES=`find . -name '*.html'`;
  for FILE in $HTML_FILES; do
    # Remove the ./ prefix and the .html suffix
    DEST_NAME=`echo $FILE | sed 's/^\.\///' | sed 's/\.html$//'`;
    echo "Uploading $FILE to s3://$WEBSITE_BUCKET/$DEST_NAME";
    aws s3 cp $FILE s3://$WEBSITE_BUCKET/$DEST_NAME --content-type text/html;
  done

  echo "Invalidating CloudFront cache...";
  aws cloudfront create-invalidation --distribution-id $CLOUDFRONT_ID --paths "/*";

  echo "Done! Website available at: $CLOUDFRONT_DOMAIN";
fi
