# Game Share
This is an app for sharing video game keys with other users that is deployed to AWS using serverless technologies. It uses the following AWS services:
- S3
- CloudFront
- Lambda
- API Gateway
- Cognito
- DynamoDB

## Setup
1. Get an API key from [RAWG](https://rawg.io/apidocs)
1. Put your API key in `/game-share/lib/rawgApi.js`
1. Create a SSM Parameter called /game-share/rawg-api-key of type `SecureString` and set the value to your RAWG API key
1. Execute ./deploy.sh
1. Wait a bit for the stack (the Cognito resources, DynamoDB GSI, and CloudFront distrubution can take a while to create)
1. The script should output the URL of the app, it will be something like https://d1q2r3s4t5.cloudfront.net
1. If you want to receive email notifications when a user signs up, you can subscribe to the SNS topic created by the stack
1. Sign up for an account using the app, then go to the AWS Cognito console to approve your user and make it an admin
1. You can now update the parameters for the stack to use the optional features


## Architecture
![Architecture](docs/architecture.png){ width=500px }

### Key Data
The partition key and sort key are used to ensure that keys are sorted by the time they were submitted. This allows us to easily query for the most recent keys and display them in the UI.
Keys are stored in DynamoDB with the following schema:
- Partition key: `key_id` - A unique identifier for the key
- Sort Key: `submitted_at` - The timestamp of when the key was submitted
- Attributes:
  - `game_name` - The name of the game the key is for
  - `key_value` - The key itself
  - `key_type` - The type of key (full game, DLC, etc.)
  - `notes` - Any notes the user added when submitting the key
  - `platform` - The platform the key is for (Steam, Epic, etc.)
  - `submitted_by` - The user id of the user that submitted the key
  - `submitted_by_name` - The name of the user that submitted the key
  - `external_api_id` - The id of the key in the external API, if applicable, for querying game information
  - `claimed_by` - The user id of the user that claimed the key
  - `claimed_by_name` - The name of the user that claimed the key
  - `claimed_at` - The timestamp of when the key was claimed
  - `availability` - The status of the key, can be one of the following:
    - `AVAILABLE` - The key is available to be claimed
    - `CLAIMED` - The key has been claimed
  - `genres` - The genres of the game, pulled from RAWG, added by the DynamoDB stream when a new key is submitted or the external_api_id is updated
  - `metacritic_score` - The metacritic score of the game, pulled from RAWG, added by the DynamoDB stream when a new key is submitted or the external_api_id is updated
  - `rating` - The rating of the game, pulled from RAWG, added by the DynamoDB stream when a new key is submitted or the external_api_id is updated
  - `tags` - The tags of the game, pulled from RAWG, added by the DynamoDB stream when a new key is submitted or the external_api_id is updated
  - `rawg_game` - The full game object from RAWG, added by the DynamoDB stream when a new key is submitted or the external_api_id is updated
  - `rawg_screenshots` - The screenshots of the game from RAWG, added by the DynamoDB stream when a new key is submitted or the external_api_id is updated
  - `rawg_stores` - The stores of the game from RAWG,, added by the DynamoDB stream when a new key is submitted or the external_api_id is updated

### API Endpoints
#### Keys API
- `GET /api/keys` - Publically available - Returns all keys that are available to be claimed
- `POST /api/keys` - User must be approved - Creates a new key
  - Required fields:
    - `key_value`
    - `game_name`
    - `key_type`
    - `platform`
    - `notes`
    - `external_api_id`
- `GET /api/keys/:id` - Publically available - Returns the key with the specified id
- `DELETE /api/keys/:id` - Must be the user who submitted the key or an admin - Deletes the key with the specified id
- `PUT /api/keys/:id` - User must be approved - Updates the key with the specified id (only the original submitter or an admin can update the key)
  - Required fields:
    - `key_value`
    - `game_name`
    - `key_type`
    - `platform`
    - `notes`
    - `external_api_id`
- `GET /api/keys/:id/secure` - Must be the user who submitted the key or an admin - Returns the key with the specified id, including the key_value (used for editing existing keys)
- `GET /api/keys/:id/claim` - Must be the user who claimed the key or an admin - Returns the key_value for the key with the specified id, if it has been claimed
- `POST /api/keys/:id/claim` - User must be approved - Claims the key with the specified id, returns the actual key
- `GET /api/keys/recent-claims` - User must be an admin - Returns the most recent keys that have been claimed
- `GET /api/keys/my-claims` - User must be approved - Returns all keys that the user has claimed

#### Users API
- `GET /api/users` - User must be an admin - Returns all users
- `GET /api/users/:id/name` - Publically available - Returns the user's name with the specified id
- `POST /api/users/:id/approve` - User must be an admin - Approves the user with the specified id

### Page Mapping
- `/` - The home page, shows all the available keys to claim, searchable and filterable
- `/about` - The about page
- `/my-claims` - The page for viewing all the keys you have claimed
- `/key/submit` - The page for submitting a new key
- `/key/detail?key_id=KEY_ID_HERE` - The page for viewing a specific key
- `/key/edit?key_id=KEY_ID_HERE` - The page for editing a specific key
- `/admin`
  - `/admin/bulk-upload` - The page for bulk uploading keys from a csv
  - `/admin/generate-fake-keys` - The page for generating fake keys for testing
  - `/admin/recent-claims` - The page for viewing the most recent keys that have been claimed and by whom
  - `/admin/view-users` - The page for viewing all users and approving new users

![Page Mapping](docs/sitemap.png){ width=500px }

### Optional Features
#### Custom Domain
1. Register a domain in [Route53](https://us-east-1.console.aws.amazon.com/route53/domains/home?region=us-east-1#/DomainSearch), you have to wait until [the request](https://us-east-1.console.aws.amazon.com/route53/domains/home?region=us-east-1#/ListRequests) is successfully processed. This can take several minutes to several hours depending on the TLD.
1. Update the parameters `pDomainName` to the domain you registered on the setup stack via the CloudFormation console
1. Go to the [ACM console](https://us-east-1.console.aws.amazon.com/acm/home?region=us-east-1#/certificates/list) and confirm the certificate request that the stack requested by opening the certificate request and clicking the "Create record in Route 53" button. Usually within a minute, the certificate should create and the stack will continue.
1. Once the stack has fully updated, you should see the application hosted on your domain

## Pricing Notes
In general, this application should be free or very cheap to run. These are the pricing dimensions for each service and how they are used by this app:
- CloudFront has a [free tier](https://aws.amazon.com/cloudfront/pricing/) that should cover all usage of this app (1TB of data out and 10 million HTTP/S requests per month)
- S3 has a [free tier](https://aws.amazon.com/s3/pricing/) that should cover all usage of this app (5GB of storage and 20,000 GET requests per month which are cached by CloudFront)
- API Gateway has a [free tier](https://aws.amazon.com/api-gateway/pricing/) that should cover all usage of this app (1 million requests per month)
- Lambda has a [free tier](https://aws.amazon.com/lambda/pricing/) that should cover all usage of this app (1 million requests and 400,000 GB-seconds per month)
- Cognito has a [free tier](https://aws.amazon.com/cognito/pricing/) that should cover all usage of this app (50,000 monthly active users)
- DynamoDB has a [free tier](https://aws.amazon.com/dynamodb/pricing/) that should cover most of this app (25GB of storage), since we are using on-demand capacity, there will be a small charge (pennies, if that) for the reads and writes, the DynamoDB Streams is also free for the first 2.5 million reads per month
- SSM Parameter Store doesn't charge for standard parameters, which is what we are using

If you configure a custom domain, additional charges may apply:
- Pricing for domains vary (usually $10-15) per year
- Route53 has a [free tier](https://aws.amazon.com/route53/pricing/) that should cover all usage of this app (1 hosted zone and 25 record sets), additional hosted zones are $0.50 per month

## Local Development
After executing ./deploy.sh, you can run a few different flavors of the app locally when in the aws-serverless-notes directory:
- `npm run dev` - Run the app in development mode using nextjs's dev server, features hot releoads
- `npm run build` - Build the app for production, this saves the output to the ./out directory
- `npm run serve` - Run the app out of the ./out directory, this is the should be the same as what is deployed to AWS

Weird note, you can't have the app running in dev mode (ie `npm run dev`) and execute a build (either `npm run build` or `./deploy.sh app`) at the same time. I don't know why but it causes the build to fail.

## deploy.sh
This script handles the full deployment of the app. It will:
- Deploy the AWS setup stack
- Pull several outputs from the setup stack and use them to configure the app
- Build the app for production
- Deploy the app to S3

The script can be ran multiple times, it will update the setup stack and the app stack as needed. It assumes you are building the app in the simplest way possible, which is to say you are not using a custom domain or Google OAuth. You can update the parameters of the stack to use these features, but the parameters will not be updated by the script so you should only need to set them once.

If you are iterating on a specific feature, you can pass the operation to the deploy script to skip the steps you don't need. Valid options are:
- `./deploy.sh aws` - Deploy the AWS setup stack
- `./deploy.sh app` - Build and deploy the app to S3, also invalidates the CloudFront cache
- `./deploy.sh amplify-config` - Pull the outputs from the AWS setup stack and updates the amplify config
- `./deploy.sh all` - Deploy the AWS setup stack, build and deploy the app to S3, and invalidate the CloudFront cache, this is the default operation if no operation is specified

## Google OAuth Setup
Not fully implemented, these steps don't work yet

1. Deploy the AWS setup stack with no parameters for Google OAuth, copy the Cognito domain name from the outputs
1. Create a new google project https://console.developers.google.com/
1. On the left pane, select OAuth consent screen
1. Fill out required fields, add 'amazoncognito.com' as an authorized domain
1. Select the email, profile, and openid scopes
1. Add your google email as a test account
1. Finish the wizard
1. Under the credentials page, create a new OAuth 2.0 credential of type Web application
1. Enter the Cognito domain name in the redirect url
1. Copy the client id and secret
1. Update the stack in AWS and specify the id and secret from the previous step

## References:
- [NextJS static exporting](https://nextjs.org/docs/pages/building-your-application/deploying/static-exports)
- [AWS Amplify Authenticator](https://ui.docs.amplify.aws/react/connected-components/authenticator)
- [RAWG API Docs](https://api.rawg.io/docs/)
- Shoutout to Github Copilot and ChatGPT
