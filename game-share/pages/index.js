import Head from 'next/head';


import Layout from '@/components/Layout';
import GameList from '@/components/GameList';

export default function Home() {

  return (
    <>
      <Head>
        <title>Game Share</title>
      </Head>
      <Layout>
        <h2>Available Games</h2>
        <GameList />
      </Layout>
    </>
  )
}
