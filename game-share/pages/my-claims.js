import { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { formatDistanceToNow } from 'date-fns';

import { UserContext } from '@/contexts/UserContext';
import Layout from '@/components/Layout';
import { getMyClaims } from '@/lib/api';
import clickableTableStyles from '@/styles/clickableTable.module.css';

export default function UserClaims() {
  const router = useRouter();

  const {user, setUser} = useContext(UserContext);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [claims, setClaims] = useState([]);

  // On page load, fetch all users
  useEffect(() => {
    const fetchUsersClaims = async () => {
      setIsLoading(true);
      const [claims, error] = await getMyClaims();
      if (error) {
        console.log('Error fetching claims:', error);
        setError(error);
      } else {
        setClaims(claims.sort((a, b) => {
          return new Date(b.claimed_at) - new Date(a.claimed_at);
        }));
      }
      setIsLoading(false);
    };
    if (user) {
      fetchUsersClaims();
    }
  }, [user]);

  return (
    <>
      <Head>
        <title>Game Share - My Claims</title>
      </Head>
      <Layout>
        <h2>Your claimed keys</h2>
        <div>
          {error && (
            <p>Error fetching claims: {JSON.stringify(error)}</p>
          )}
          {!user && (
            <p>You must be logged in to view this page</p>
          )}
          {user && isLoading && (
            <p>Loading...</p>
          )}
          {user && !isLoading && (
            <>
              {claims.length === 0 && (
                <p>You haven&apos;t claimed any keys yet</p>
              )}
              {claims.length > 0 && (
                <>
                  <p>Click on a row to view the details of the key, you can retrieve the key data on the key&apos;s detail page</p>
                  <table className={clickableTableStyles.table}>
                    <thead className={clickableTableStyles.header}>
                      <tr>
                        <th>Game Name</th>
                        <th>Platform</th>
                        <th>Key Type</th>
                        <th>Submitted</th>
                        <th>Claimed</th>
                      </tr>
                    </thead>
                    <tbody>
                      {claims.map(claim => {
                        return (
                          <tr key={claim.key_id} className={clickableTableStyles.row} onClick={() => router.push(`/key/detail?key_id=${claim.key_id}`)}>
                            <td><span className={clickableTableStyles.mobileLabel}>Game Name: </span>{claim.game_name}</td>
                            <td><span className={clickableTableStyles.mobileLabel}>Platform: </span>{claim.platform}</td>
                            <td><span className={clickableTableStyles.mobileLabel}>Type: </span>{claim.key_type}</td>
                            <td title={new Date(claim.submitted_at).toLocaleString()}><span className={clickableTableStyles.mobileLabel}>Submitted: </span>{claim.submitted_by_name} ({formatDistanceToNow(new Date(claim.submitted_at))} ago)</td>
                            <td title={new Date(claim.claimed_at).toLocaleString()}><span className={clickableTableStyles.mobileLabel}>Claimed: </span>{claim.claimed_by_name} ({formatDistanceToNow(new Date(claim.claimed_at))} ago)</td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </table>
                </>
              )}
            </>
          )}
        </div>
      </Layout>
    </>
  )
}
