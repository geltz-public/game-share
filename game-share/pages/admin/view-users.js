import { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import { formatDistanceToNow, set } from 'date-fns';

import { UserContext } from '@/contexts/UserContext';
import Layout from '@/components/Layout';
import { getAllUsersApi, approveUserApi } from '@/lib/api'

export default function ViewUsers() {
  const router = useRouter();

  const {user, setUser} = useContext(UserContext);
  const [isLoading, setIsLoading] = useState(false);
  const [users, setUsers] = useState([]);
  const [error, setError] = useState(null);

  // On page load, fetch all users
  useEffect(() => {
    const fetchUsers = async () => {
      setIsLoading(true);
      const [users, error] = await getAllUsersApi();
      if (error) {
        console.log('Error fetching users: ' + JSON.stringify(error));
        setError(error);
      } else {
        setUsers(users.sort((a, b) => {
          return new Date(b.UserLastModifiedDate) - new Date(a.UserLastModifiedDate);
        }));
      }
      setIsLoading(false);
    };

    if (user) {
      const isAdmin = user?.attributes?.['custom:admin'];
      console.log('Checking if user is an admin:', isAdmin ?? "false")
      
      if (isAdmin !== "true") {
        console.log('User is not an admin, redirecting to home page');
        router.push('/');
      } else {
        fetchUsers();
      }
    }
  }, [user, router]);

  const getAttributeValue = (user, attributeName) => {
    const attributeItem = user.Attributes.find(item => item.Name === attributeName);
    if (attributeItem) {
      return attributeItem.Value;
    }
    return null;
  };

  const approveUser = async (user) => {
    // If the user is already approved, don't do anything
    const isApproved = getAttributeValue(user, 'custom:approved');
    if (isApproved === 'true') {
      console.log('User is already approved, not approving again');
      return;
    }
  
    console.log('Approving user with id ' + user.Username);
    const [result, error] = await approveUserApi(user.Username);
    if (error) {
      console.log('Error approving user: ' + JSON.stringify(error));
      setError(error);
    } else {
      console.log('Successfully approved user: ' + JSON.stringify(result));
      const updatedUsers = users.map(currentUser => {
        if (currentUser.Username !== user.Username) return currentUser; // return unmodified user
        
        // Create a new array of attributes with the updated approved status
        const newUserAttributes = [
          ...currentUser.Attributes,
          { Name: 'custom:approved', Value: 'true' }
        ];
  
        return { ...currentUser, Attributes: newUserAttributes };
      });
  
      setUsers(updatedUsers);
    }
  };
  

  return (
    <>
      <Head>
        <title>Game Share - User Admin</title>
      </Head>
      <Layout>
        <h2>View Users</h2>
        {error && <div>Error fetching users: {error?.message}</div>}
        {(!user && !isLoading) && (
          <div>You are not logged in</div>
        )}
        {(user && isLoading) && (
          <div>Loading...</div>
        )}
        {(user && !isLoading) && (
          <div>
            <table>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Approved</th>
                  <th>Approve</th>
                  <th>Admin</th>
                  <th>Last modified</th>
                  <th>Enabled</th>
                  <th>User status</th>
                </tr>
              </thead>
              <tbody>
                {users.map((user) => (
                  <tr key={user.Username}>
                    <td>{getAttributeValue(user, 'name')}</td>
                    <td>{getAttributeValue(user, 'email')}</td>
                    <td>{(getAttributeValue(user, 'custom:approved') ?? "false") === "true" ? "Approved" : "Not approved"}</td>
                    <td>{(getAttributeValue(user, 'custom:approved') !== "true") ? (<button onClick={() => approveUser(user)}>Approve</button>) : `N/A` }</td>
                    <td>{(getAttributeValue(user, 'custom:admin') ?? "false") === "true" ? "Admin" : "Not admin"}</td>
                    <td title={new Date(user?.UserLastModifiedDate).toLocaleString()}>{formatDistanceToNow(new Date(user?.UserLastModifiedDate))} ago</td>
                    <td>{user?.Enabled ? "Enabled" : "Disabled"}</td>
                    <td>{user?.UserStatus}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
      </Layout>
    </>
  )
}
