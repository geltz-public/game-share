import { useState, useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Papa from 'papaparse';

import CSVUploader from '@/components/CSVUploader';
import Layout from '@/components/Layout';
import { UserContext } from '@/contexts/UserContext';
import { KEY_TYPES, PLATFORMS } from '@/lib/constants';
import { submitNewKeyApi } from '@/lib/api';
import { searchGamesByName } from '@/lib/rawgApi';
import clickableTableStyles from '@/styles/clickableTable.module.css';

export default function BulkSubmissionPage() {
  const router = useRouter();

  const { user, setUser } = useContext(UserContext);
  const [csvData, setCsvData] = useState([]);
  const [allowSubmit, setAllowSubmit] = useState(false);
  const [status, setStatus] = useState('Waiting for upload');
  const [errors, setErrors] = useState([]);
  const [submittedKeys, setSubmittedKeys] = useState([]);

  useEffect(() => {
    if (user) {
      const isAdmin = user?.attributes?.['custom:admin'];
      console.log('Checking if user is an admin:', isAdmin ?? "false")

      if (isAdmin !== "true") {
        console.log('User is not an admin, redirecting to home page');
        router.push('/');
      }
    }
  }, [user, router]);

  const handleUpload = (file) => {
    console.log('File uploaded:', file);
    setStatus('Uploading file...');
    setAllowSubmit(false);
    setErrors([]);
    Papa.parse(file, {
      complete: (result) => {
        console.log('Parsed Result:', result);
        checkIfCanSubmit(result.data);
      },
      header: true,
      dynamicTyping: true,
    });
  };

  const requiredFields = [
    'key_value',
    'game_name',
    'key_type',
    'platform',
  ]

  const checkIfCanSubmit = (data) => {
    setCsvData(data);
    setStatus('Checking for errors...');

    let foundErrors = [];
    if (data.length === 0) {
      setAllowSubmit(false);
      setStatus('No data found in file');
      foundErrors.push('No data found in file');
      setErrors(foundErrors);
      return;
    }

    for (const [i, row] of data.entries()) {
      const gameName = row?.game_name ?? 'Unknown';
      // Check if required fields are present and valid
      for (let field of requiredFields) {
        if (!row[field]) {
          foundErrors.push(`Missing required field ${field} for game '${gameName}' on row ${i+1}`);
        } else if (row[field] === '') {
          foundErrors.push(`Empty value for required field ${field} for game ${gameName} on row ${i+1}`);
        } else if (field === 'key_type' && !KEY_TYPES.includes(row[field])) {
          foundErrors.push(`Invalid key_type '${row.key_type}' for game '${gameName}' on row ${i+1}`);
        } else if (field === 'platform' && !PLATFORMS.includes(row[field])) {
          foundErrors.push(`Invalid platform '${row.platform}' for game '${gameName}' on row ${i+1}`);
        } else if (field === 'genre' && !GENRES.includes(row[field])) {
          foundErrors.push(`Invalid genre '${row.genre}' for game '${gameName}' on row ${i+1}`);
        }
      }
      if (!row.notes) {
        // If notes is empty, set it to an empty string
        row.notes = '';
        console.log('Notes is empty, setting to empty string');
      }
    }
    
    if (foundErrors.length > 0) {
      setAllowSubmit(false);
      setStatus('Errors found in file');
      setErrors(foundErrors);
    } else {
      populateGameMatches(data);
      setErrors([]);
    }
  };

  const populateGameMatches = async (data) => {
    setStatus('Populating game matches from RAWG API...');
    let errors = [];
    for (let row of data) {
      const gameName = row.game_name;
      const [result, error] = await searchGamesByName(gameName, 1);
      if (error) {
        console.log('Error searching for game:', error);
        errors.push(`Error searching for game '${gameName}': ${error}`);
      } else {
        console.log('Search result for game:', result);
        if (result.count === 0) {
          console.log('No game found for:', gameName);
          errors.push(`No game found for '${gameName}'`);
        } else {
          const game = result.results[0];
          console.log('Game found:', game);
          row.external_api_id = `${game.id}`;
        }
      }
    }

    if (errors.length > 0) {
      setAllowSubmit(false);
      setStatus('Errors populating RAWG game matches');
      setErrors(errors);
    } else {
      setAllowSubmit(true);
      setStatus('Ready to submit');
    }
  };

  const handleSubmit = async () => {
    setSubmittedKeys([]);
    let keys = [];

    for (const [i, row] of csvData.entries()) {
      const gameName = row.game_name;
      setStatus(`Submitting key ${i+1} of ${csvData.length} for game '${gameName}'...`);
      const [result, error] = await submitNewKeyApi(row);
      if (error) {
        console.log('Error submitting key:', error);
        setErrors([`Error submitting key for game '${gameName}': ${error}`]);
        return;
      }
      console.log('Successfully submitted key:', result);
      keys.push(result);
    }

    setSubmittedKeys(keys);
    setCsvData([]);
    setAllowSubmit(false);
    setStatus('Keys processed successfully!');
  };

  return (
    <>
      <Head>
        <title>Game Share - Bulk Submission</title>
      </Head>
      <Layout>
        <h2>Bulk Submission</h2>
        {!user && (
          <div>You are not logged in</div>
        )}
        {user && (
          <>
            <CSVUploader onUpload={handleUpload} />
            <div>Status: {status}</div>
          </>
        )}
        {errors.length > 0 && (
          <div>
            <h2>Errors</h2>
            <ul>
              {errors.map((error, index) => (
                <li key={index}>{error}</li>
              ))}
            </ul>
          </div>
        )}
        {csvData.length > 0 && (
          <div>
            <h2>Preview of CSV data</h2>
            <table>
              <thead>
                <tr>
                  <th>Row</th>
                  <th>Key Value</th>
                  <th>Game Name</th>
                  <th>Key Type</th>
                  <th>Platform</th>
                  <th>Notes</th>
                  <th>External API ID</th>
                </tr>
              </thead>
              <tbody>
                {csvData.map((row, index) => (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{row.key_value}</td>
                    <td>{row.game_name}</td>
                    <td>{row.key_type}</td>
                    <td>{row.platform}</td>
                    <td>{row.notes}</td>
                    <td>{row.external_api_id}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
        {csvData.length > 0 && !allowSubmit && (
          <div>Please fix any errors before proceeding</div>
        )}
        {allowSubmit && (
          <button onClick={handleSubmit}>Process Keys</button>
        )}
        {submittedKeys.length > 0 && (
          <div>
            <h2>Submitted Keys</h2>
            <table className={clickableTableStyles.table}>
              <thead className={clickableTableStyles.header}>
                <tr>
                  <th>Key Value</th>
                  <th>Game Name</th>
                  <th>Key Type</th>
                  <th>Platform</th>
                  <th>Notes</th>
                  <th>External API ID</th>
                </tr>
              </thead>
              <tbody>
                {submittedKeys.map((key, index) => (
                  <tr key={index} className={clickableTableStyles.row} onClick={() => router.push(`/key/detail?key_id=${key.key_id}`)}>
                    <td>{key.key_value}</td>
                    <td>{key.game_name}</td>
                    <td>{key.key_type}</td>
                    <td>{key.platform}</td>
                    <td>{key.notes}</td>
                    <td>{key.external_api_id}</td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
      </Layout>
    </>
  );
}
