import { useEffect, useContext, useState } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';
import Chance from 'chance';

import Layout from '@/components/Layout';
import { UserContext } from '@/contexts/UserContext';
import { submitNewKeyApi } from '@/lib/api';
import { getRandomGame } from '@/lib/rawgApi';
import { PLATFORMS } from '@/lib/constants';

const chance = new Chance();

const Admin = () => {
  const router = useRouter();

  const { user, setUser } = useContext(UserContext);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [submittedKeyData, setSubmittedKeyData] = useState(null);
  const [numKeysToGenerate, setNumKeysToGenerate] = useState(1);
  const [currentlyGeneratingKey, setCurrentlyGeneratingKey] = useState(null);

  useEffect(() => {
    if (user) {
      const isAdmin = user?.attributes?.['custom:admin'];
      console.log('Checking if user is an admin:', isAdmin ?? "false")

      if (isAdmin !== "true") {
        console.log('User is not an admin, redirecting to home page');
        router.push('/');
      }
    }
  }, [user, router]);

  const generateKeyPayloadFromRawgData = async (rawgGameInfo) => {
    // Find if one of the platforms on the game is in our list of platforms, otherwise pick a random one
    let platform = chance.pickone(PLATFORMS);
    if (rawgGameInfo.stores && rawgGameInfo.stores.length > 0) {
      const matchingPlatform = rawgGameInfo.stores.find(store => PLATFORMS.includes(store.store.name));
      if (matchingPlatform) {
        console.log("Found matching platform on game's RAWG info:", matchingPlatform);
        platform = matchingPlatform.store.name;
      }
    }
    return {
      key_value: chance.string({ length: 15, pool: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-' }),
      game_name: rawgGameInfo.name,  // Use the game name from the API result
      key_type: 'Game',
      platform: platform,
      notes: chance.sentence({ words: 6 }),
      external_api_id: `${rawgGameInfo.id || chance.guid()}`
    };
  };

  const generateOneGame = async () => {
    const [game, error] = await getRandomGame();
    if (error) {
      console.error('Error fetching random game:', error);
      setError(error);
      setIsLoading(false);
      return null;
    }
    if (game && game.results && game.results.length > 0) {
      const randomGame = chance.pickone(game.results);
      console.log("Randomly selected game from RAWG:", randomGame);
      return generateKeyPayloadFromRawgData(randomGame);
    } else {
      console.warn('No games found');
      setError('No games found');
      return null;
    }
  };

  const handleGenerateAndSubmit = async () => {
    setIsLoading(true);
    setError(null);
    setSubmittedKeyData(null);
    const numKeys = parseInt(numKeysToGenerate);
    let submittedKeys = [];
    for (let i = 0; i < numKeys; i++) {
      setCurrentlyGeneratingKey(i + 1);
      const key = await generateOneGame();
      if (key) {
        const [result, error] = await submitNewKeyApi(key);
        if (error) {
          console.error('Error submitting the key:', error);
        } else {
          console.log('Key submitted successfully!', result);
          submittedKeys.push(result);
        }
      } else {
        console.error('Error generating key, skipping');
      }
    }
    setCurrentlyGeneratingKey(null);
    setSubmittedKeyData(submittedKeys);
    setIsLoading(false);
  };

  return (
    <>
      <Head>
        <title>Game Share - Admin - Fake Key Generator</title>
      </Head>
      <Layout>
        <div>
          <h1>Admin - Generate Fake Keys</h1>
          {!user && (
            <div>You are not logged in</div>
          )}
          {user && (
            <>
              <p>Use this page to generate fake keys for testing purposes.</p>
              <p>How many keys would you like to generate?</p>
              <select value={numKeysToGenerate} onChange={(e) => setNumKeysToGenerate(e.target.value)}>
                <option value="1">1</option>
                <option value="5">5</option>
                <option value="25">25</option>
                <option value="50">50</option>
              </select>
              <button onClick={handleGenerateAndSubmit}>
                Generate and Submit Fake Key(s)
              </button>
              {error && <div>Error generating fake key: {error?.message}</div>}
              {currentlyGeneratingKey && (
                <div>Generating key {currentlyGeneratingKey} of {numKeysToGenerate}...</div>
              )}
              {isLoading ? (
                <div>Loading...</div>
              ) : (
                <div>
                  {submittedKeyData && (
                    <div>
                      <h2>Submitted Key(s)</h2>
                      <pre>{JSON.stringify(submittedKeyData, null, 2)}</pre>
                    </div>
                  )}
                </div>
              )}
            </>
          )}
        </div>
      </Layout>
    </>
  );
};

export default Admin;
