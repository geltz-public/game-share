import { useContext, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { formatDistanceToNow } from 'date-fns';

import { UserContext } from '@/contexts/UserContext';
import Layout from '@/components/Layout';
import { getRecentClaims, getUserName } from '@/lib/api';
import clickableTableStyles from '@/styles/clickableTable.module.css';

export default function RecentClaims() {
  const router = useRouter();

  const {user, setUser} = useContext(UserContext);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [claims, setClaims] = useState([]);

  useEffect(() => {
    const fetchRecentClaims = async () => {
      setIsLoading(true);
      const [claims, error] = await getRecentClaims();
      if (error) {
        console.log('Error fetching claims: ' + JSON.stringify(error));
        setError(error);
      } else {
        console.log('Got claims: ' + JSON.stringify(claims));
        setClaims(claims.sort((a, b) => {
          return new Date(b.claimed_at) - new Date(a.claimed_at);
        }));
      }
      setIsLoading(false);
    };

    if (user) {
      const isAdmin = user?.attributes?.['custom:admin'];
      console.log('Checking if user is an admin:', isAdmin ?? "false")
      
      if (isAdmin !== "true") {
        console.log('User is not an admin, redirecting to home page');
        router.push('/');
      } else {
        fetchRecentClaims();
      }
    }
  }, [user, router]);

  return (
    <>
      <Head>
        <title>Game Share - Redemption Admin</title>
      </Head>
      <Layout>
        <h2>View Recent Redemptions</h2>
        <div>
          {error && (
            <p>Error loading recent redemptions: {JSON.stringify(error)}</p>
          )}
          {!user && !isLoading && (
            <p>You are not logged in</p>
          )}
          {user && isLoading && (
            <p>Loading...</p>
          )}
          {user && !isLoading && (
            <>
              <table className={clickableTableStyles.table}>
                <thead className={clickableTableStyles.header}>
                  <tr>
                    <th>Game Name</th>
                    <th>Platform</th>
                    <th>Key Type</th>
                    <th>Submitted</th>
                    <th>Claimed</th>
                  </tr>
                </thead>
                <tbody>
                  {claims.map(claim => {
                    return (
                      <tr key={claim.key_id} className={clickableTableStyles.row} onClick={() => router.push(`/key/detail?key_id=${claim.key_id}`)}>
                        <td>{claim.game_name}</td>
                        <td>{claim.platform}</td>
                        <td>{claim.key_type}</td>
                        <td title={new Date(claim.submitted_at).toLocaleString()}>{claim.submitted_by_name} ({formatDistanceToNow(new Date(claim.submitted_at))} ago)</td>
                        <td title={new Date(claim.claimed_at).toLocaleString()}>{claim.claimed_by_name} ({formatDistanceToNow(new Date(claim.claimed_at))} ago)</td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </>
          )}
        </div>
      </Layout>
    </>
  )
}
