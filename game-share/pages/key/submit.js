import { useContext } from 'react';
import Head from 'next/head';

import { UserContext } from '@/contexts/UserContext';
import Layout from '@/components/Layout';
import KeyForm from '@/components/KeyForm';

const Submit = function () {
  const {user, setUser} = useContext(UserContext);

  return (
    <>
      <Head>
        <title>Game Share - Submit a Key</title>
      </Head>
      <Layout>
        <h2>Submit a new key</h2>
        {!user && (
          <p>You must be logged in to view this page</p>
        )}
        {user && (
          <KeyForm isEditMode={false} />
        )}
      </Layout>
    </>
  )
}

export default Submit;
