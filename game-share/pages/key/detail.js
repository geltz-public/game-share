import { useState, useEffect, useContext } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { formatDistanceToNow, set } from 'date-fns';

import Layout from '@/components/Layout';
import Modal from '@/components/Modal';
import Carousel from '@/components/Carousel';
import styles from '@/styles/KeyDetails.module.css';
import { getSpecificKeyApi, claimKeyApi, getClaimedKeyValue, deleteKeyApi } from '@/lib/api';
import { UserContext } from '@/contexts/UserContext';

const KeyDetails = function () {
  const router = useRouter();

  const { user, setUser } = useContext(UserContext);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(null);
  const [key, setKey] = useState(null);
  const [showClaimConfirmationModal, setShowClaimConfirmationModal] = useState(false);
  const [showKeyValueModal, setShowKeyValueModal] = useState(false);
  const [showDeleteConfirmModal, setShowDeleteConfirmModal] = useState(false);
  const [claimedKeyValue, setClaimedKeyValue] = useState(null);
  const [rawgInfo, setRawgInfo] = useState(null);
  const [rawgScreenshots, setRawgScreenshots] = useState(null);
  const [rawgStores, setRawgStores] = useState(null);

  useEffect(() => {
    const fetchKeyData = async () => {
      setIsLoading(true);
      const [response, apiError] = await getSpecificKeyApi(router.query.key_id);
      setKey(response);
      setError(apiError);
      setIsLoading(false);
      if (response.claimed_by && response.claimed_by == user?.username) {
        console.log('User has claimed this key, fetching key value');
        getKeyValue(response?.key_id);
      }
      if (response?.external_api_id && response.rawg_game) {
        // Load RAWG data from key
        const rawgGameInfo = JSON.parse(response.rawg_game);
        setRawgInfo(rawgGameInfo);
        console.log('Rawg game info:', rawgGameInfo);
        const rawgScreenshots = JSON.parse(response.rawg_screenshots);
        setRawgScreenshots(rawgScreenshots);
        console.log('Rawg screenshots:', rawgScreenshots);
        const rawgStores = JSON.parse(response.rawg_stores);
        console.log('Rawg stores:', rawgStores);
        setRawgStores(rawgStores);
      }
    };

    if (router.query.key_id) {
      console.log('Key id found in query params:', router.query.key_id);
      fetchKeyData();
    } else {
      console.log('No key id found in query params, displaying error');
      setError('No key id found in query params');
    }
  }, [router.query.key_id]); // eslint-disable-line react-hooks/exhaustive-deps

  const handleClaim = async () => {
    const [keyValue, error] = await claimKeyApi(key.key_id);
    if (error) {
      console.error('Error claiming key:', error);
      setError('Error claiming the key. Please try again later.');
      setShowClaimConfirmationModal(false);
      return;
    }
    setKey(keyValue);
    setClaimedKeyValue(keyValue?.key_value);
    setShowClaimConfirmationModal(false);
    setShowKeyValueModal(true);
  };

  const getKeyValue = async (key_id) => {
    const [keyValue, error] = await getClaimedKeyValue(key_id);
    if (error) {
      console.error('Error retrieving key value:', error);
      setError('Error retrieving the key value. Please try again later.');
    } else {
      setClaimedKeyValue(keyValue?.key_value);
    }
  };

  const deleteKey = async (key_id) => {
    console.log('Deleting key with id', key_id);
    const [result, error] = await deleteKeyApi(key_id);
    if (error) {
      console.error('Error deleting key:', error);
      setError('Error deleting the key. Please try again later.');
    } else {
      console.log('Key deleted successfully!', result);
      router.push('/');
    }
  };

  const pagetitle = function () {
    if (key && key.game_name) {
      return <title>Game Share - {key.game_name} - Key Details</title>;
    } else {
      return <title>Game Share - Key Details</title>;
    }
  }

  return (
    <>
      <Head>
        {pagetitle()}
      </Head>
      <Layout>
        {isLoading && <p>Loading...</p>}
        {error && <p>Error: {error?.message}</p>}
        {!isLoading && key && (
          <>
            <h2>{key.game_name}</h2>
            <div className={styles.keyDetailsContainer}>
              <div className={styles.keyDetails}>
                {/* Key Details Table */}
                <table>
                  <tbody>
                    <tr>
                      <td>Platform:</td>
                      <td>{key.platform}</td>
                    </tr>
                    {key.genres && key.genres.length > 0 && (
                      <tr>
                        <td>Genres:</td>
                        <td>{key.genres.join(', ')}</td>
                      </tr>
                    )}
                    <tr>
                      <td>Key Type:</td>
                      <td>{key.key_type}</td>
                    </tr>
                    {key.notes && (
                      <tr>
                        <td>Notes:</td>
                        <td>{key.notes }</td>
                      </tr>
                    )}
                    <tr>
                      <td>Submitted by:</td>
                      <td>{key.submitted_by_name}</td>
                    </tr>
                    <tr>
                      <td>Submitted:</td>
                      <td title={new Date(key.submitted_at).toLocaleString()}>
                        {formatDistanceToNow(new Date(key.submitted_at))} ago
                      </td>
                    </tr>
                    {key.claimed_at && (
                      <tr>
                        <td>Claimed:</td>
                        <td title={new Date(key.claimed_at).toLocaleString()}>
                          {formatDistanceToNow(new Date(key.claimed_at))} ago by {key.claimed_by_name}
                        </td>
                      </tr>
                    )}
                    <tr>
                      <td>Availability:</td>
                      <td>{key.availability === "AVAILABLE" ? "Available" : "Claimed"}</td>
                    </tr>
                    <tr>
                      <td>Actions:</td>
                      <td>
                        {key.availability === "AVAILABLE" && !user && (
                          <strong>You must be logged in to claim keys</strong>
                        )}
                        {key.availability === "AVAILABLE" && user && user?.attributes?.['custom:approved'] !== "true" && (
                          <strong>Your account is still awaiting approval</strong>
                        )}
                        {key.availability === "AVAILABLE" && user?.attributes?.['custom:approved'] === "true" && (
                          <button onClick={() => setShowClaimConfirmationModal(true)}>Claim Key</button>
                        )}
                        {key.availability === "AVAILABLE" && (key.submitted_by == user?.username || user?.attributes?.['custom:admin'] === "true") && (
                          <button onClick={() => router.push(`/key/edit?key_id=${key.key_id}`)}>Edit</button>
                        )}
                        {key.availability === "AVAILABLE" && (key.submitted_by == user?.username || user?.attributes?.['custom:admin'] === "true") && (
                          <button onClick={() => setShowDeleteConfirmModal(true)}>Delete</button>
                        )}
                        {key.availability === "CLAIMED" && key.claimed_by == user?.username && (
                          <button onClick={() => setShowKeyValueModal(true)}>Show key</button>
                        )}
                        {key.availability === "CLAIMED" && key.claimed_by != user?.username && (
                          <span>No actions available</span>
                        )}
                      </td>
                    </tr>
                  </tbody>
                </table>
                <div className={styles.rightSection}>
                  {rawgScreenshots && <Carousel images={rawgScreenshots.map(screenshot => screenshot.image)} />}
                </div>
              </div>
            </div>
            {key.external_api_id && !rawgInfo && (
              <p>Game metadata still processing, refresh in a few moments for more info</p>
            )}
            {rawgInfo && (
              <div className={styles.rawgInfo}>
                <table>
                  <tbody>
                    {rawgInfo.publishers && (
                      <tr>
                        <td>Publishers:</td>
                        <td>{rawgInfo.publishers.map(publisher => publisher.name).join(', ')}</td>
                      </tr>
                    )}
                    {rawgInfo.released && (
                      <tr>
                        <td>Release Date:</td>
                        <td>{rawgInfo.released}</td>
                      </tr>
                    )}
                    {key.rating && (
                      <tr>
                        <td>RAWG Rating:</td>
                        <td>{key.rating}/5 {rawgInfo.ratings_count ? `(${rawgInfo.ratings_count} ratings)` : undefined}</td>
                      </tr>
                    )}
                    {rawgInfo.metacritic && (
                      <tr>
                        <td>Metacritic Score:</td>
                        <td>
                          {rawgInfo.metacritic_url && (
                            (<a href={rawgInfo.metacritic_url} target="_blank" rel="noopener noreferrer">{rawgInfo.metacritic}</a>)
                          )}
                          {!rawgInfo.metacritic_url && (
                            <span>{rawgInfo.metacritic}/100</span>
                          )}
                        </td>
                      </tr>
                    )}
                    {rawgInfo.esrb_rating && (
                      <tr>
                        <td>ESRB Rating:</td>
                        <td>{rawgInfo.esrb_rating.name}</td>
                      </tr>
                    )}
                    {rawgInfo.website && (
                      <tr>
                        <td>Website:</td>
                        <td><a href={rawgInfo.website} target="_blank" rel="noopener noreferrer">{rawgInfo.website}</a></td>
                      </tr>
                    )}
                    {rawgInfo.stores && rawgStores && (
                      <tr>
                        <td>Buy on:</td>
                        <td>
                          {rawgInfo.stores.map(store_in_info => {
                            const storeInfo = rawgStores.find((store) => store.store_id === store_in_info.store.id);
                            if (!storeInfo) {
                              console.log('Store info not found for store id', store_in_info);
                              return null;
                            };
                            return (
                              <a key={store_in_info.store.id} href={storeInfo.url} target="_blank" rel="noopener noreferrer">
                                [{store_in_info.store.name}]
                              </a>
                            );
                          })}
                        </td>
                      </tr>
                    )}
                  </tbody>
                </table>
                <div className={styles.gameDescription}>
                  <h3>About the game:</h3>
                  <p>{rawgInfo.description_raw}</p>
                </div>
                <div>
                  <h3>Tags</h3>
                  <p>{
                    rawgInfo.tags.sort(
                      (a, b) => a.games_count > b.games_count ? -1 : 1
                    )
                    .map(tag => tag.name)
                    .join(', ')}
                  </p>
                </div>
              </div>
            )}
          </>
        )}
        <Modal
          isOpen={showClaimConfirmationModal}
          onRequestClose={() => setShowClaimConfirmationModal(false)}
          title="Are you sure?"
          description="Claiming the key is irreversible"
          type="two-button"
          buttonText="Confirm"
          buttonAction={handleClaim}
          secondaryButtonText="Cancel"
          secondaryButtonAction={() => setShowClaimConfirmationModal(false)}
        />
        <Modal
          isOpen={showKeyValueModal}
          onRequestClose={() => setShowKeyValueModal(false)}
          title="Key claimed!"
          description={`Key: ${claimedKeyValue}`}
          type="one-button"
          buttonText="Okay"
          buttonAction={() => setShowKeyValueModal(false)}
        />
        <Modal
          isOpen={showDeleteConfirmModal}
          onRequestClose={() => setShowDeleteConfirmModal(false)}
          title="Are you sure?"
          description="Deleting the key is irreversible"
          type="two-button"
          buttonText="Delete"
          buttonAction={() => deleteKey(key.key_id)}
          secondaryButtonText="Cancel"
          secondaryButtonAction={() => setShowDeleteConfirmModal(false)}
        />
      </Layout>
    </>
  )
}

export default KeyDetails;
