import { useEffect, useState, useContext } from 'react';
import { useRouter } from 'next/router';
import Head from 'next/head';

import { UserContext } from '@/contexts/UserContext';
import Layout from '@/components/Layout';
import KeyForm from '@/components/KeyForm';
import { getSpecificKeyIncludingValueApi } from '@/lib/api';

const EditKey = () => {
  const router = useRouter();

  const {user, setUser} = useContext(UserContext);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [keyData, setKeyData] = useState({});

  useEffect(() => {
    const fetchKeyData = async () => {
      setIsLoading(true);
      setError(null);
      const [data, error] = await getSpecificKeyIncludingValueApi(router.query.key_id);
      if (error) {
        console.error('Error fetching key data:', error);
        setError(error);
      }
      setKeyData(data);
      setIsLoading(false);
    };

    if (user) {
      if (router.query.key_id) {
        console.log('Fetching key data for key_id:', router.query.key_id);
        fetchKeyData();
      } else {
        console.log('No key_id found in router.query:', router.query);
        setError('No key_id found in path, please try again')
      }
    }
  }, [user, router.query]);

  return (
    <>
      <Head>
        <title>Game Share - Edit Key</title>
      </Head>
      <Layout>
        <h2>Edit Key</h2>
        {error && (
          <p>Error fetching key data: {error?.message}</p>
        )}
        {!user && (
          <p>You must be logged in to view this page</p>
        )}
        {user && isLoading && (
          <p>Loading...</p>
        )}
        {user && !isLoading && keyData && (
          <KeyForm isEditMode={true} initialData={keyData} />
        )}
      </Layout>
    </>
  )
}

export default EditKey;
