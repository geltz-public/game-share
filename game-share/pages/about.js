import { useContext } from 'react';
import Head from 'next/head';

import { UserContext } from '../contexts/UserContext';
import Layout from '../components/Layout';

export default function About() {
  const { user, setUser } = useContext(UserContext);

  return (
    <>
      <Head>
        <title>Game Share - About</title>
      </Head>
      <Layout>
        <div>
          <h2>About Game Share</h2>

          <p>
            <strong>Game Share</strong> is a platform designed for gaming enthusiasts to share and exchange video game keys seamlessly. Our platform empowers users to distribute their unused game keys, ensuring that everyone has an opportunity to explore a wider variety of titles.
          </p>

          <h2>How to Start Claiming or Submitting Keys:</h2>

          <ol>
            <li>Sign up for an account
              <ul>
                <li>A valid email is required to confirm your account</li>
                <li>Ensure to use your real name so an admin knows who you are</li>
              </ul>
            </li>
            <li>Wait for an admin to approve your account</li>
            <li>Once approved, you can start claiming keys or submitting your own keys</li>
          </ol>

          <h2>Features:</h2>

          <h3>1. Game Key Management:</h3>
          <ul>
            <li>Users can share game keys they don&apos;t want. These keys can be sorted and filtered to find keys of interest to you.</li>
            <li>Game keys cover a range of platforms like Steam and Epic and can be of different types (full game, DLC, etc.).</li>
            <li>Each key contains metadata provided by the sharer, such as the game&apos;s genre, any notes, and the submission time.</li>
            <li>Keys can be linked to data from the RAWG API, which enhances the key&apos;s page with additional game facts, screenshots, and descriptions.</li>
            <li>Users can edit or delete keys they have submitted.</li>
            <li>Approved users can claim keys they want. Once claimed, the key is hidden from the platform.</li>
            <li>Admin users can view recently claimed keys, ensuring smooth oversight of the platform&apos;s key exchange process.</li>
          </ul>

          <h3>2. User Profiles & Approval System:</h3>
          <ul>
            <li>The application features a user approval system. Only users approved by an admin can share or claim keys.</li>
            <li>Publicly accessible user names ensure anonymity, while also allowing the community to track key submissions.</li>
          </ul>

          <h2>Technical Insights:</h2>

          <p>
            Game Share&apos;s architecture leverages the serverless capabilities of AWS, optimizing scalability, and ensuring reliable performance.
          </p>
          <ul>
            <li><strong>Frontend</strong>: The frontend is built with <strong>Next.js</strong> and <strong>React</strong>, ensuring a smooth user experience. Exporting the frontend as a static site allows for easy deployment and hosting.</li>
            <li><strong>Hosting & Distribution</strong>: The core services include <strong>S3</strong> for storing game keys and <strong>CloudFront</strong> for efficient content distribution.</li>
            <li><strong>Serverless Computing</strong>: With <strong>Lambda</strong>, we can execute backend operations without maintaining a server, ensuring cost-effectiveness and smooth scalability.</li>
            <li><strong>Secure & Scalable Database</strong>: <strong>DynamoDB</strong> handles our data storage needs.</li>
            <li><strong>Authentication</strong>: The React <strong>Amplify</strong> framework handles user authentication and authorization on the frontend.</li>
            <li><strong>User Management</strong>: <strong>Cognito</strong> facilitates secure user sign-up and sign-in. It also integrates seamlessly with the platform&apos;s user approval system.</li>
            <li><strong>API Management</strong>: <strong>API Gateway</strong> manages the various API endpoints, ensuring secure and efficient data flow.</li>
          </ul>
          <p>
            Check out our source code on Gitlab <a href="https://gitlab.com/geltz-public/game-share">here</a>.
          </p>

          <h2>Why Game Share is Impressive:</h2>

          <ul>
            <li><strong>Serverless Advantage</strong>: The application uses a serverless architecture, ensuring optimum scalability without the traditional server maintenance overhead.</li>
            <li><strong>Cost-Efficiency</strong>: Game Share is designed to operate primarily within AWS&apos;s free tier limits, ensuring the platform is affordable.</li>
              <ul>
                <li>CloudFront has a <a href="https://aws.amazon.com/cloudfront/pricing/">free tier</a> that should cover all usage of this app (1TB of data out and 10 million HTTP/S requests per month)</li>
                <li>S3 has a <a href="https://aws.amazon.com/s3/pricing/">free tier</a> that should cover all usage of this app (5GB of storage and 20,000 GET requests per month which are cached by CloudFront)</li>
                <li>API Gateway has a <a href="https://aws.amazon.com/api-gateway/pricing/">free tier</a> that should cover all usage of this app (1 million requests per month)</li>
                <li>Lambda has a <a href="https://aws.amazon.com/lambda/pricing/">free tier</a> that should cover all usage of this app (1 million requests and 400,000 GB-seconds per month)</li>
                <li>Cognito has a <a href="https://aws.amazon.com/cognito/pricing/">free tier</a> that should cover all usage of this app (50,000 monthly active users)</li>
                <li>DynamoDB has a <a href="https://aws.amazon.com/dynamodb/pricing/">free tier</a> that should cover most of this app (25GB of storage), since we are using on-demand capacity, there will be a small charge (pennies, if that) for the reads and writes</li>
              </ul>
            <li><strong>High-Quality Game Data Integration</strong>: The integration with <a href="https://rawg.io/apidocs">RAWG API</a> ensures that game details are always up-to-date and reliable.</li>
          </ul>
        </div>
      </Layout>
    </>
  )
}
