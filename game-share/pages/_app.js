import Amplify from '@aws-amplify/core';

import '@/styles/globals.css'
import config from './amplify-config.json';

import { UserProvider } from '../contexts/UserContext';

Amplify.configure({
  Auth: {
      region: config.region,
      userPoolId: config.userPoolId,
      userPoolWebClientId: config.userPoolWebClientId,
  },
  API: {
    endpoints: [
      {
        name: 'KeyApi',
        endpoint: config.apiEndpoint,
        region: config.region,
        authorizationType: 'AMAZON_COGNITO_USER_POOLS',
      },
    ]
  }
});

export default function App({ Component, pageProps }) {
  return (
    <UserProvider>
      <Component {...pageProps} />
    </UserProvider>
  );
}
