const RAWG_API = 'https://api.rawg.io/api';
const RAWG_API_KEY = '9b3d6882da4d45c3b7252e508a223a6b';

async function fetchFromRAWG(method, endpoint, options = {}) {
  let url = `${RAWG_API}/${endpoint}?key=${RAWG_API_KEY}`;
  // Add options to url if there are any
  if (Object.keys(options).length > 0) {
    url += '&' + Object.keys(options).map(key => `${key}=${options[key]}`).join('&');
  }
  const request_body = {
    headers: {
      'User-Agent': 'Game Share App'
    }
  }
  console.log('Invoking RAWG API:', method, url);
  const response = await fetch(url, request_body);
  if (!response.ok) {
    console.error('Error fetching from RAWG API:', response);
    return [ null, response ];
  }
  const result = await response.json();
  console.log('RAWG API response for:', method, url, 'Result:', result);
  return [ result, null ];
}

export async function searchGamesByName(query, limit=5) {
  return fetchFromRAWG("GET", 'games', { search: query, page_size: limit });
}

export async function getGameById(gameId) {
  return fetchFromRAWG("GET", `games/${gameId}`);
}

export async function getGameScreenshots(gameId) {
  return fetchFromRAWG("GET", `games/${gameId}/screenshots`);
}

export async function getGameStores(gameId) {
  return fetchFromRAWG("GET", `games/${gameId}/stores`);
}

export async function getRandomGame() {
  const randomNumber = Math.floor(Math.random() * 1000);
  return fetchFromRAWG("GET", 'games', { page_size: 1, page: randomNumber });
}
