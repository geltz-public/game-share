export const KEY_TYPES = [
  'Game',
  'DLC',
  'Expansion',
  'Bundle',
  'Season Pass',
  'Beta',
  'Other'
]

export const PLATFORMS = [
  'Steam',
  'BattleNet',
  'EA/Origin',
  'Epic Games Store',
  'GOG',
  'Humble (non-steam)',
  'Itch.io',
  'Ubisoft',
  'Xbox (PC)',
  'PlayStation Store',
  'Nintendo Store',
  'Xbox (console)',
  'Other'
];