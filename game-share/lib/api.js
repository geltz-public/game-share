import { Amplify } from '@aws-amplify/core';
import { API } from 'aws-amplify';

async function invokeApi(method, apiName, path, authenticated = false, body = null) {
  let request = {};
  if (body) {
    request.body = body;
  }
  if (authenticated) {
    request.headers = {
      Authorization: `Bearer ${(await Amplify.Auth.currentSession()).getIdToken().getJwtToken()}`
    };
  }
  console.log('Invoking API:', apiName, method, path, 'body:', body);
  try {
    let result;
    if (method === "GET") {
      result = await API.get(apiName, path, request);
    } else if (method === "POST") {
      result = await API.post(apiName, path, request);
    } else if (method === "PUT") {
      result = await API.put(apiName, path, request);
    } else if (method === "DELETE") {
      result = await API.del(apiName, path, request);
    } else {
      throw new Error(`Invalid method: ${method}`);
    }
    console.log('API response for:', apiName, method, path, 'Response:', result);
    return [ result, null ];
  } catch (error) {
    console.log('Error invoking API:', error);
    console.log('Error response:', error?.response);
    return [ null, error ];
  }
}

/* Key API */

export async function getAvailableKeysApi() {
  const apiName = 'KeyApi';
  const path = '/keys';
  try {
    // Paginate through all keys
    const keys = [];
    let nextToken = null;
    const maxPages = 10;
    let pages = 0;
    do {
      const request = nextToken ? { queryStringParameters: { NextToken: nextToken } } : {};
      const result = await API.get(apiName, path, request);
      console.log('Got a page of keys from the API of', result.keys.length, 'keys');
      keys.push(...result.keys);
      nextToken = result.nextToken;
      pages++;
      console.log('Next token:', nextToken, 'Pages:', pages, 'Max pages:', maxPages);
    } while (nextToken && pages < maxPages);
    console.log('Got full key list (', keys.length, ') from the API:', keys);
    return [ keys, null ];
  } catch (error) {
    if (error?.response?.status === 404) {
      console.log('No keys found');
      return [ [], null ];
    }
    console.log('Error fetching keys: ', error);
    console.log('Error response: ', error?.response);
    return [ [], error ];
  }
}

export async function getSpecificKeyApi(keyId) {
  const apiName = 'KeyApi';
  const path = `/keys/${keyId}`;
  return invokeApi("GET", apiName, path);
};

export async function getSpecificKeyIncludingValueApi(keyId) {
  const apiName = 'KeyApi';
  const path = `/keys/${keyId}/secure`;
  return invokeApi("GET", apiName, path, true);
}

export async function submitNewKeyApi(newKey) {
  const apiName = 'KeyApi';
  const path = '/keys';
  return invokeApi("POST", apiName, path, true, newKey);
};

export async function claimKeyApi(keyId) {
  const apiName = 'KeyApi';
  const path = `/keys/${keyId}/claim`;
  return invokeApi("POST", apiName, path, true);
};

export async function getClaimedKeyValue(keyId) {
  const apiName = 'KeyApi';
  const path = `/keys/${keyId}/claim`;
  return invokeApi("GET", apiName, path, true);
};

export async function updateKeyApi(updatedKey) {
  const apiName = 'KeyApi';
  const path = `/keys/${updatedKey.key_id}`;
  return invokeApi("PUT", apiName, path, true, updatedKey);
};

export async function deleteKeyApi(keyId) {
  const apiName = 'KeyApi';
  const path = `/keys/${keyId}`;
  return invokeApi("DELETE", apiName, path, true);
};


/* User API */

export async function getUserName(userId) {
  const apiName = 'KeyApi';
  const path = `/users/${userId}`;
  return invokeApi("GET", apiName, path);
}

export async function getAllUsersApi() {
  const apiName = 'KeyApi';
  const path = '/users';
  return invokeApi("GET", apiName, path, true);
}

export async function approveUserApi(userId) {
  const apiName = 'KeyApi';
  const path = `/users/${userId}/approve`;
  return invokeApi("PUT", apiName, path, true);
};


/* Claim API */

export async function getRecentClaims() {
  const apiName = 'KeyApi';
  const path = '/keys/recent-claims';
  return invokeApi("GET", apiName, path, true);
};

export async function getMyClaims() {
  const apiName = 'KeyApi';
  const path = '/keys/my-claims';
  return invokeApi("GET", apiName, path, true);
};