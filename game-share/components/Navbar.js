import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faExclamationTriangle, faBars, faChevronDown, faChevronUp } from '@fortawesome/free-solid-svg-icons'

import styles from './Navbar.module.css';

const Navbar = ({ user, handleSignInClick, handleSignOutClick }) => {
  const router = useRouter();

  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [isAdminMenuOpen, setIsAdminMenuOpen] = useState(false);

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  const toggleAdminMenu = () => {
    setIsAdminMenuOpen(!isAdminMenuOpen);
  };

  useEffect(() => {
    const handleClickOutsideAdminMenu = (e) => {
      if (!e.target.closest(`.${styles.adminMenu}`)) {
        setIsAdminMenuOpen(false);
      }
    };
    document.addEventListener('click', handleClickOutsideAdminMenu);
    return () => {
      document.removeEventListener('click', handleClickOutsideAdminMenu);
    };
  }, []);

  const AdminDropdownMenu = () => {
    return (
      <div className={styles.adminDropdown}>
        <NavMenuItem href="/admin/view-users" displayText="View users" />
        <NavMenuItem href="/admin/recent-claims" displayText="Recent claims" />
        <NavMenuItem href="/admin/bulk-upload" displayText="Bulk upload" />
        <NavMenuItem href="/admin/generate-fake-keys" displayText="Generate fake keys" />
      </div>
    );
  };

  const UserStateIcon = () => {
    const isUserVerified = user?.attributes?.['custom:approved'] ?? "false";
    const tooltipText = isUserVerified === "true" ? "Approved to redeem keys" : "Not yet verified, you will be unable to submit or redeem keys";
    return (
      <div title={tooltipText}>
        {isUserVerified == "true" ? (
          <FontAwesomeIcon icon={faCheck} color="green" />
        ) : (
          <FontAwesomeIcon icon={faExclamationTriangle} color="yellow" />
        )}
      </div>
    );
  };

  const appendClassConditionally = (baseClass, condition, classToAdd) => {
    return `${baseClass}${condition ? ' ' + classToAdd : ''}`;
  };

  const NavMenuItem = ({ href, displayText }) => {
    const isCurrentPage = router.pathname === href;
    const className = appendClassConditionally(styles.navLink, isCurrentPage, styles.currentPageLink)
    return (
      <Link href={href} className={className}>{displayText}</Link>
    );
  };

  return (
    <nav className={styles.navbar}>
      <div className={styles.navMobileToggle} onClick={toggleMenu}>
        <FontAwesomeIcon icon={faBars} color="white" />
      </div>
      <div className={appendClassConditionally(styles.navLeft, isMenuOpen, styles.mobile)}>
        <NavMenuItem href="/" displayText="Home" />
        {user?.attributes?.['custom:approved'] === "true" && (
          <NavMenuItem href="/key/submit" displayText="Submit a key" />
        )}
        {user?.attributes?.['custom:approved'] === "true" && (
          <NavMenuItem href="/my-claims" displayText="My claimed keys" />
        )}
        {user?.attributes?.['custom:admin'] === "true" && (
          <div onClick={toggleAdminMenu} className={styles.adminMenu}>
            Admin panel
            <FontAwesomeIcon
              icon={isAdminMenuOpen ? faChevronUp : faChevronDown}
              className={styles.adminMenuIcon}
            />
            {isAdminMenuOpen && <AdminDropdownMenu />}
          </div>
        )}

        <NavMenuItem href="/about" displayText="About" />
      </div>
      <div className={appendClassConditionally(styles.navRight, isMenuOpen, styles.mobile)}>
        {!user && (
          <>
            <button onClick={handleSignInClick} className={styles.navButton}>Sign In/Up</button>
          </>
        )}
        {user && (
          <div className={`${styles.userContainer}`}>
            <span className={styles.userName}>Hello, {user.attributes.name}</span>
            <UserStateIcon />
          </div>
        )}
        {user && (
          <button onClick={handleSignOutClick} className={styles.navButton}>Sign Out</button>
        )}
      </div>
    </nav>
  );
}

export default Navbar;
