import { useState, useEffect } from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import styles from './GameMatchModal.module.css';
import { searchGamesByName } from '@/lib/rawgApi';

function GameMatchModal({
  gameName,
  isOpen,
  onRequestClose,
  continueAction,
}) {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState(null);
  const [searchTerm, setSearchTerm] = useState(gameName);
  const [searchResults, setSearchResults] = useState([]);
  const [selectedGameId, setSelectedGameId] = useState(null);

  useEffect(() => {
    setSearchTerm(gameName);
    if (isOpen && gameName) {
      updateResults(gameName);
    }
  }, [gameName, isOpen]);

  const handleSearchChange = async (event) => {
    const { value } = event.target;
    setSearchTerm(value);
  };

  const handleSelectGameButtonClick = () => {
    if (selectedGameId) {
      console.log('User is proceeding with match id:', selectedGameId);
      continueAction(`${selectedGameId}`);
    } else {
      console.log('User clicked "Select this game" without selecting a game');
    }
  };

  const selectGame = (gameId) => {
    console.log('User selected game with id:', gameId);
    setSelectedGameId(gameId);
  };

  const updateResults = async (nameToSearch) => {
    console.log('Searching for games with name:', nameToSearch);
    setIsLoading(true);
    setError(null);
    setSelectedGameId(null);
    const [results, error] = await searchGamesByName(nameToSearch);
    setIsLoading(false);
    if (error) {
      console.error('Error searching for games:', error);
      setError(error);
      setSearchResults([]);
      return;
    }
    console.log('Got search results:', results);
    setSearchResults(results);
  };

  // If the modal is not open, don't render anything
  if (!isOpen) return null;
  return (
    <div className={styles.modalOverlay}>
      <div className={styles.modal}>
        <button className={styles.closeButton} onClick={onRequestClose}>
          <FontAwesomeIcon icon={faTimes} />
        </button>
        <h3>Find a match for the game</h3>
        <p>Is this the game you want to submit a key for?</p>
        <input type="text" name="game_name" value={searchTerm} onChange={handleSearchChange} />
        <button onClick={() => updateResults(searchTerm)}>Search</button>
        {isLoading && <p>Loading...</p>}
        {error && <p>Error searching for games: {error.message}</p>}
        {!isLoading && searchResults?.results?.length === 0 && <p>No results found.</p>}
        {!isLoading && searchResults?.results?.length > 0 && (
          <>
            <p>Found {searchResults.results.length} results.</p>
            <div className={styles.searchResults}>
              {searchResults.results.map(game => (
                <div key={game.id} className={styles.searchResult} onClick={() => selectGame(game.id)}>
                {/* For accessibility, retaining the radio button and label */}
                <input type="radio" name="game" id={`game-${game.id}`} value={game.id} checked={selectedGameId === game.id} onChange={() => selectGame(game.id)} />
                {game.short_screenshots && game.short_screenshots.length > 0 && 
                  <img src={game.short_screenshots[0].image} alt={`Screenshot of ${game.name}`} className={styles.gameScreenshot} />
                }
                <label htmlFor={`game-${game.id}`}>{game.name} ({game.released})</label>
              </div>
              ))}
            </div>
          </>
        )}
        <div className={styles.modalButtons}>
          <button onClick={handleSelectGameButtonClick} className={!selectedGameId ? styles.disabledButton : undefined}>Select this game</button>
          <button onClick={() => continueAction(null)}>Skip matching</button>
        </div>
      </div>
    </div>
  );
}

export default GameMatchModal;
