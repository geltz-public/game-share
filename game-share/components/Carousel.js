import React, { useState, useEffect } from 'react';
import styles from './Carousel.module.css';

const Carousel = ({ images }) => {
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    // Preload images
    images.forEach(image => {
      const newImage = new Image();
      newImage.src = image;
    });
  }, [images]);

  const goPrev = () => {
    if (currentIndex === 0) setCurrentIndex(images.length - 1);
    else setCurrentIndex(currentIndex - 1);
  };

  const goNext = () => {
    if (currentIndex === images.length - 1) setCurrentIndex(0);
    else setCurrentIndex(currentIndex + 1);
  };

  return (
    <div className={styles.carouselContainer}>
      {images.length > 1 && (
        <button className={`${styles.carouselBtn} ${styles.carouselBtnPrev}`} onClick={goPrev}>&lt;</button>
      )}
      <img src={images[currentIndex]} alt={`Screenshot ${currentIndex + 1}`} className={styles.screenshot} />
      {images.length > 1 && (
        <button className={`${styles.carouselBtn} ${styles.carouselBtnNext}`} onClick={goNext}>&gt;</button>
      )}
    </div>
  );
};

export default Carousel;
