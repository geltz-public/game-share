import React, { useState, useEffect, useContext } from 'react';

import { Auth, Hub } from 'aws-amplify';
import { Authenticator } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';

import { UserContext } from '@/contexts/UserContext';
import Navbar from './Navbar';
import Banner from './Banner';

const Layout = ({ children }) => {
  const { user, setUser } = useContext(UserContext);
  const [showAuth, setShowAuth] = useState(false);

  useEffect(() => {
    const listener = async (data) => {
      if (data.payload.event === 'signIn') {
        console.log('auth event:', data?.payload?.event);
        console.log("User signed in, fetching user data...");
        setShowAuth(false);
        try {
          const userData = await Auth.currentAuthenticatedUser();
          if (userData !== user) {
            setUser(userData);
            console.log("Fetched user data:", userData);
          }
        } catch (error) {
          console.log("Error fetching user:", error);
          setUser(null);
        }
      }
    };
    // Add listener when the component mounts
    const unsubscribe = Hub.listen('auth', listener); // This now returns an unsubscribe function
    // Return a cleanup function to remove the listener when the component unmounts
    return () => {
      unsubscribe();
    };
  }, [user, setUser]);

  useEffect(() => {
    const checkUser = async () => {
      try {
        const userData = await Auth.currentAuthenticatedUser();
        setUser(userData);
        console.log("Fetched user data:", userData);
      } catch (error) {
        console.log("Error fetching user:", error);
        setUser(null);
      }
    };

    checkUser();
  }, [setUser]);

  const handleSignInClick = () => {
    console.log("User clicked sign in, showing modal");
    setShowAuth(true);
  };

  const handleSignOutClick = () => {
    console.log("User clicked sign out, clearing user state");
    Auth.signOut();
    setUser(null);
  };

  const authFormFields = {
    signUp: {
      name: {
        label: 'Full Name (used by admins for account approval):',
        placeholder: 'Enter your Name:',
        isRequired: true,
        order: 1,
      },
      email: {
        label: 'Email (requires verification):',
        placeholder: 'Enter your Email:',
        isRequired: true,
        order: 2,
      },
      password: {
        label: 'Password:',
        placeholder: 'Enter your Password:',
        isRequired: false,
        order: 3,
      },
      confirm_password: {
        label: 'Confirm Password:',
        placeholder: 'Confirm your Password:',
        isRequired: false,
        order: 4,
      },
    }
  }

  return (
    <div className="container">
      <Banner bannerText="You must be signed in to claim keys" showBanner={user === null} />
      <Banner bannerText="Your account is still awaiting approval" showBanner={user && user?.attributes?.['custom:approved'] !== "true"} />
      <Navbar
        user={user}
        handleSignInClick={handleSignInClick}
        handleSignOutClick={handleSignOutClick}
      />
      <div className="content">
        {showAuth && (
          <>
            <div style={{
              position: 'fixed',
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
              backgroundColor: 'rgba(0, 0, 0, 0.5)', // semi-transparent
              zIndex: 0.9, // just below the Authenticator
            }}></div>
            <Authenticator
              variation="modal"
              loginMechanisms={['email']}
              signUpAttributes={['name']}
              formFields={authFormFields}
            />
          </>
        )}
        <main>{children}</main>
        {/* ... */}
      </div>
    </div>
  );
};

export default Layout;