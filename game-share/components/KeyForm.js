import React, { useState } from 'react';
import { useRouter } from 'next/router';

import styles from './KeyForm.module.css';
import { submitNewKeyApi, updateKeyApi } from '@/lib/api';
import Modal from '@/components/Modal';
import GameMatchModal from '@/components/GameMatchModal';
import { PLATFORMS, KEY_TYPES } from '@/lib/constants';

const KeyForm = ({ isEditMode = false, initialData = null }) => {
  const router = useRouter();

  const [showSubmitCompleteModal, setShowSubmitCompleteModal] = useState(false);
  const [showGameMatchModal, setShowGameMatchModal] = useState(false);
  const [submittedKeyId, setSubmittedKeyId] = useState(null);

  const [formData, setFormData] = useState({
    key_id: initialData?.key_id || null,
    key_value: initialData?.key_value || '',
    game_name: initialData?.game_name || '',
    key_type: initialData?.key_type || KEY_TYPES[0],
    platform: initialData?.platform || PLATFORMS[0],
    notes: initialData?.notes || '',
    external_api_id: initialData?.external_api_id || '',
  });

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setFormData(prevState => ({
      ...prevState,
      [name]: value
    }));
  };

  const handleOpenGameMatchModal = (event) => {
    console.log('Starting game match process', formData.game_name);
    event.preventDefault();
    setShowGameMatchModal(true);
  };

  const handleSubmit = async (dataToSubmit) => {
    let result, error;
    if (isEditMode) {
      [result, error] = await updateKeyApi(dataToSubmit);
    } else {
      [result, error] = await submitNewKeyApi(dataToSubmit);
    }
    if (error) {
      console.error('Error submitting the key:', error);
      console.error('Error response:', error?.response);
    } else {
      console.log('Key submitted/updated successfully!', result);
      setSubmittedKeyId(result.key_id);
      setShowSubmitCompleteModal(true);
    }
  };

  const handleCloseModal = () => {
    setShowSubmitCompleteModal(false);
    console.log('User wanted to enter another key, clearing form data');
    // Clear the form if they choose "Submit Another Key"
    setFormData({
      key_value: '',
      game_name: '',
      key_type: KEY_TYPES[0],
      platform: PLATFORMS[0],
      notes: '',
      external_api_id: ''
    });
  };

  return (
    <>
      <form onSubmit={handleOpenGameMatchModal} className={styles.container}>
        <label className={styles.label}>
          Key:
          <input type="text" name="key_value" value={formData.key_value} onChange={handleInputChange} className={styles.input} required />
        </label>

        <label className={styles.label}>
          Game Name:
          <input type="text" name="game_name" value={formData.game_name} onChange={handleInputChange} className={styles.input} required />
        </label>

        <label className={styles.label}>
          Key Type:
          <select name="key_type" value={formData.key_type} onChange={handleInputChange} className={styles.select} required>
            {KEY_TYPES.map(keyType => (
              <option key={keyType} value={keyType}>{keyType}</option>
            ))}
          </select>
        </label>

        <label className={styles.label}>
          Platform:
          <select name="platform" value={formData.platform} onChange={handleInputChange} className={styles.select} required>
            {PLATFORMS.map(platform => (
              <option key={platform} value={platform}>{platform}</option>
            ))}
          </select>
        </label>

        <label className={styles.label}>
          Notes:
          <textarea name="notes" value={formData.notes} onChange={handleInputChange} className={styles.textarea}></textarea>
        </label>

        <button type="submit" className={styles.button}>{isEditMode ? "Update" : "Submit"} Key</button>
      </form>
      <GameMatchModal
        gameName={formData.game_name}
        isOpen={showGameMatchModal}
        onRequestClose={() => setShowGameMatchModal(false)}
        continueAction={(external_api_id) => {
          console.log('Closing match modal and submitting key with match:', external_api_id);
          setShowGameMatchModal(false);
          let dataToSubmit = formData;
          if (external_api_id) {
              console.log('Setting external_api_id to:', external_api_id);
              dataToSubmit = { ...formData, external_api_id };
          }
          console.log('Submitting key:', dataToSubmit);
          handleSubmit(dataToSubmit);
        }}
      />
      <Modal
        isOpen={showSubmitCompleteModal}
        onRequestClose={handleCloseModal}
        title="Thank you"
        description="What's next?"
        type="two-button"
        buttonText="View key details"
        buttonAction={() => router.push(`/key/detail?key_id=${submittedKeyId}`)}
        secondaryButtonText={isEditMode ? "Submit a new key" : "Submit another key"}
        secondaryButtonAction={handleCloseModal}
      />
    </>
  );
};

export default KeyForm;
