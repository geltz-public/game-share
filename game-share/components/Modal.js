import React from 'react';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import styles from './Modal.module.css';

function Modal({
  isOpen,
  onRequestClose,
  title,
  description,
  type, // "one-button" or "two-button"
  buttonText, // Text for the main button
  buttonAction, // Action for the main button
  secondaryButtonText, // Text for the secondary button (only for "two-button" type)
  secondaryButtonAction, // Action for the secondary button (only for "two-button" type)
}) {
  // If the modal is not open, don't render anything
  if (!isOpen) return null;

  return (
    <div className={styles.modalOverlay}>
      <div className={styles.modal}>
        <button className={styles.closeButton} onClick={onRequestClose}>
          <FontAwesomeIcon icon={faTimes} />
        </button>
        <h3>{title}</h3>
        {description && <p>{description}</p>}
        {type === 'one-button' && (
          <button onClick={buttonAction}>{buttonText}</button>
        )}
        {type === 'two-button' && (
          <div className={styles.modalButtons}>
            <button onClick={buttonAction}>{buttonText}</button>
            <button onClick={secondaryButtonAction}>
              {secondaryButtonText}
            </button>
          </div>
        )}
      </div>
    </div>
  );
}

export default Modal;
