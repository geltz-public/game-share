export default function Banner({ bannerText, showBanner }) {
  return (
    <>
      {showBanner && (
        <div style={bannerStyle}>
          {bannerText}
        </div>
      )}
    </>
  );
}

const bannerStyle = {
  position: 'fixed',
  top: 0,
  left: 0,
  width: '100%',
  backgroundColor: 'var(--warning)',
  color: 'black',
  textAlign: 'center',
  padding: '5px 0',
  zIndex: 0.3,
};
