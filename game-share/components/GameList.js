import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { formatDistanceToNow } from 'date-fns';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faSteam,
  faBattleNet,
  faItchIo,
  faWindows, // Represents Xbox (PC)
  faXbox,
  faPlaystation
} from '@fortawesome/free-brands-svg-icons';

import styles from './GameList.module.css';
import clickableTableStyles from '@/styles/clickableTable.module.css';
import { getAvailableKeysApi } from '../lib/api';

const GameList = function () {
  const router = useRouter();

  const [isLoading, setIsLoading] = useState(false);
  const [keys, setKeys] = useState([]);
  const [error, setError] = useState(null);
  const [searchTerm, setSearchTerm] = useState("");
  const [sortOption, setSortOption] = useState("newest_first");
  const [selectedPlatform, setSelectedPlatform] = useState("");
  const [selectedGenre, setSelectedGenre] = useState("");
  const [selectedKeyType, setSelectedKeyType] = useState("");

  useEffect(() => {
    const fetchKeys = async () => {
      setIsLoading(true);
      const [response, error] = await getAvailableKeysApi();
      setKeys(response);
      setError(error);
      setIsLoading(false);
    };

    fetchKeys();
  }, []);

  const handleSearchChange = (event) => {
    setSearchTerm(event.target.value);
    console.log("User searched for " + event.target.value);
  };

  const handleSortChange = (event) => {
    setSortOption(event.target.value);
    console.log("User sorted by " + event.target.value);
  };

  const handlePlatformChange = (event) => {
    setSelectedPlatform(event.target.value);
    console.log("User filtered to platform " + event.target.value);
  };

  const handleKeyTypeChange = (event) => {
    setSelectedKeyType(event.target.value);
    console.log("User filtered to key type " + event.target.value);
  };

  const handleGenreChange = (event) => {
    setSelectedGenre(event.target.value);
    console.log("User filtered to genre " + event.target.value);
  };

  const handleRowMouseAction = (e, keyId) => {
    console.log("User clicked on key with id " + keyId + " with button " + e?.button);
    if (e.button === 0 || e.button === 1) { // Left click = 0, Middle click = 1
      window.open(`/key/detail?key_id=${keyId}`, "_blank");
    }
  };

  const PlatformIcon = ({platform}) => {
    switch (platform) {
      case "Steam":
        return <FontAwesomeIcon icon={faSteam} color="var(--primary)" title={platform} />;
      case "BattleNet":
        return <FontAwesomeIcon icon={faBattleNet} color="var(--primary)" title={platform} />;
      case "Epic Games Store":
        return <span title={platform}>EGS</span>;
      case "Humble (non-steam)":
        return <span title={platform}>Humble</span>;
      case "Itch.io":
        return <FontAwesomeIcon icon={faItchIo} color="var(--primary)" title={platform} />;
      case "Xbox (PC)":
        return <FontAwesomeIcon icon={faWindows} color="var(--primary)" title={platform} />;
      case "Xbox (console)":
        return <FontAwesomeIcon icon={faXbox} color="var(--primary)" title={platform} />;
      case "PlayStation Store":
        return <FontAwesomeIcon icon={faPlaystation} color="var(--primary)" title={platform} />;
      case "Nintendo Store":
        return <span title={platform}>Nintendo</span>;
      default:
        return <span title={platform}>{platform}</span>;
    }
  };

  const filteredKeys = keys
    .filter(key => key.game_name.toLowerCase().includes(searchTerm.toLowerCase()))
    .filter(key => !selectedPlatform || key.platform === selectedPlatform)
    .filter(key => !selectedKeyType || key.key_type === selectedKeyType)
    .filter(key => !selectedGenre || key.genres?.includes(selectedGenre)) // Optional because keys won't have genres if no external_api_id is provided
    .sort((a, b) => {
      if (sortOption) {
        if (sortOption === "game_name_a_z") {
          return a.game_name.localeCompare(b.game_name);
        } else if (sortOption === "game_name_z_a") {
          return b.game_name.localeCompare(a.game_name);
        } else if (sortOption === "oldest_first") {
          return new Date(a.submitted_at) - new Date(b.submitted_at);
        } else if (sortOption === "newest_first") {
          return new Date(b.submitted_at) - new Date(a.submitted_at);
        } else if (sortOption === "RAWG_rating_high_low") {
          return b.rating - a.rating;
        } else if (sortOption === "RAWG_rating_low_high") {
          return a.rating - b.rating;
        } else if (sortOption === "metacritic_rating_high_low") {
          // If metacritic is "None", sort it to the bottom
          if (a.metacritic === "None") return 1;
          if (b.metacritic === "None") return -1;
          return b.metacritic - a.metacritic;
        } else if (sortOption === "metacritic_rating_low_high") {
          // If metacritic is "None", sort it to the bottom
          if (a.metacritic === "None") return 1;
          if (b.metacritic === "None") return -1;
          return a.metacritic - b.metacritic;
        }
      }
      return 0;
    });

  // For the sake of simplicity, I'm using a set to get unique platforms and genres. This might not be the most efficient way.
  const platforms = [...new Set(keys.map(key => key.platform))];
  const key_types = [...new Set(keys.map(key => key.key_type))];
  const genres = [...new Set(keys.map(key => key.genres).flat())];

  return (
    <>
      {isLoading && <p>Loading...</p>}
      {error && <p>Error fetching keys: {error.message}</p>}
      {!isLoading && !error && keys.length === 0 && <p>No keys available</p>}
      {!isLoading && !error && keys.length > 0 && (
        <div>
          <p>There are {keys.length} keys available</p>
          <div>
            <input
              type="text"
              placeholder="Search by game name..."
              value={searchTerm}
              onChange={handleSearchChange}
              className={styles.input}
            />
            <select value={selectedPlatform} onChange={handlePlatformChange} className={styles.dropdown}>
              <option value="">All Platforms</option>
              {platforms.map(platform => (
                <option key={platform} value={platform}>{platform}</option>
              ))}
            </select>
            <select value={selectedKeyType} onChange={handleKeyTypeChange} className={styles.dropdown}>
              <option value="">All Types of Keys</option>
              {key_types.map(key_type => (
                <option key={key_type} value={key_type}>{key_type}</option>
              ))}
            </select>
            <select value={selectedGenre} onChange={handleGenreChange} className={styles.dropdown}>
              <option value="">All Genres</option>
              {genres.map(genre => (
                <option key={genre} value={genre}>{genre}</option>
              ))}
            </select>
            <select value={sortOption} onChange={handleSortChange} className={styles.dropdown}>
              <option value="newest_first">Newest First</option>
              <option value="oldest_first">Oldest First</option>
              <option value="game_name_a_z">Game Name A-Z</option>
              <option value="game_name_z_a">Game Name Z-A</option>
              <option value="RAWG_rating_high_low">RAWG Rating High-Low</option>
              <option value="RAWG_rating_low_high">RAWG Rating Low-High</option>
              <option value="metacritic_rating_high_low">Metacritic Rating High-Low</option>
              <option value="metacritic_rating_low_high">Metacritic Rating Low-High</option>
            </select>
          </div>

          <table className={clickableTableStyles.table}>
            <thead className={clickableTableStyles.header}>
              <tr>
                <th>Game Name</th>
                <th>Genres</th>
                <th>Rating</th>
                <th>Key Type</th>
                <th>Submitted</th>
              </tr>
            </thead>
            <tbody>
              {filteredKeys.map(key => (
                <tr key={key.key_id} className={clickableTableStyles.row} onMouseDown={(e) => handleRowMouseAction(e, key.key_id)}>
                  <td>
                    <span className={clickableTableStyles.mobileLabel}>Game Name: </span>{key.game_name}
                  </td>
                  <td>
                    <span className={clickableTableStyles.mobileLabel}>Genres: </span>{key.genres?.length > 0 ? key?.genres?.join(", ") : "-"}
                  </td>
                  <td>
                    <span className={clickableTableStyles.mobileLabel}>Rating: </span>
                    {!key.rating || key.rating == '0.0' ? "-" : `${key.rating}/5`} | {!key.metacritic || key.metacritic == 'None' ? "-" : `${key.metacritic}/100`}
                  </td>
                  <td>
                    <span className={clickableTableStyles.mobileLabel}>Key Type: </span><PlatformIcon platform={key.platform} /> {key.key_type}
                  </td>
                  <td>
                    <span className={clickableTableStyles.mobileLabel}>Submitted: </span>
                    <span title={new Date(key.submitted_at).toLocaleString()}>
                      {formatDistanceToNow(new Date(key.submitted_at))} ago
                    </span>
                  </td>
                </tr>
              ))}
            </tbody>


          </table>
        </div>
      )}
    </>
  )
};

export default GameList;
