import { useDropzone } from 'react-dropzone';

export default function CSVUploader({ onUpload }) {
  const { getRootProps, getInputProps } = useDropzone({
    accept: '.csv',
    onDrop: async (acceptedFiles) => {
      onUpload(acceptedFiles[0]);
    }
  });

  const style = {
    border: '2px dotted var(--primary)',
    backgroundColor: 'var(--background-lighter',
    padding: '20px',
    textAlign: 'center',
    cursor: 'pointer',
    width: '100%', // Takes up the width of the content
    boxSizing: 'border-box', // To make sure the border is included in the width
    borderRadius: '4px' // Adds a little rounding to the corners
  };

  return (
    <div {...getRootProps()} style={style}>
      <input {...getInputProps()} />
      <p>Drag & drop your CSV file here, or click to select one</p>
    </div>
  );
}
